const path = require('path');

module.exports = [
  { name: 'PageHeader', src: path.join(__dirname, '../src/components/PageHeader.jsx') },
];
