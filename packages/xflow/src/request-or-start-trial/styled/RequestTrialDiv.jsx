import styled from 'styled-components';

import { colors } from '@atlaskit/theme';

const RequestTrialDiv = styled.div`
  color: ${colors.N800};
  width: 354px;
`;

RequestTrialDiv.displayName = 'RequestTrialDiv';
export default RequestTrialDiv;
