import styled from 'styled-components';

const StartTrialFooter = styled.div`
  text-align: right;
`;

StartTrialFooter.displayName = 'StartTrialFooter';
export default StartTrialFooter;
