// @flow
/* eslint-disable react/sort-comp, react/no-multi-comp */
import React, { Component } from 'react';
import { FocusLock, withRenderTarget } from '@atlaskit/layer-manager';
import Layer from '@atlaskit/layer';
import { layers } from '@atlaskit/theme';

import type { ActionsType, ChildrenType, ComponentType, ElementType } from '../types';
import { Dialog, DialogBody, FillScreen, Heading, Image } from '../styled/Dialog';
import { TargetOverlay, TargetOuter, TargetInner } from '../styled/Target';
import { Fade } from './Animation';
import Actions from './SpotlightActions';
import withScrollMeasurements from '../hoc/withScrollMeasurements';

const Fill = props => <Fade component={FillScreen} {...props} />;

type Props = {|
  /** Buttons to render in the footer */
  actions?: ActionsType,
  /** An optional element rendered beside the footer actions */
  actionsBeforeElement?: ElementType,
  /** The elements rendered in the modal */
  children: ChildrenType,
  /** Where the dialog should appear, relative to the contents of the children. */
  dialogPlacement?: 'top left' | 'top center' | 'top right' | 'right top' | 'right middle' | 'right bottom' | 'bottom left' | 'bottom center' | 'bottom right' | 'left top' | 'left middle' | 'left bottom',
  /** The width of the dialog in pixels. Min 160 - Max 600 */
  dialogWidth?: number,
  /** Optional element rendered below the body */
  footer?: ElementType,
  /** Optional element rendered above the body */
  header?: ElementType,
  /** Heading text rendered above the body */
  heading?: string,
  /** Path to the the your image */
  image?: string,
  /** Whether or not to display a pulse animation around the spotlighted element */
  pulse?: bool,
  /** The name of the SpotlightTarget */
  target: string,
  /** The background color of the element being highlighted */
  targetBgColor?: string,
  /** Function to fire when a user clicks on the cloned target */
  targetOnClick?: ({ event: MouseEvent, target: string }) => void,
  /** The border-radius of the element being highlighted */
  targetRadius?: number,
  /** Alternative element to render than the wrapped target */
  targetReplacement?: ComponentType,
|};
type State = {|
  isExiting: boolean,
|};

/* eslint-disable react/prop-types, react/no-danger */
const Clone = ({ html }) => (
  <div
    dangerouslySetInnerHTML={{ __html: html }}
    style={{ pointerEvents: 'none' }}
  />
);
/* eslint-enable react/prop-types, react/no-danger */

class Spotlight extends Component {
  props: Props;
  state: State = { isExiting: false };

  static defaultProps = {
    dialogWidth: 400,
    pulse: true,
  };

  handleTargetClick = (event) => {
    const { targetOnClick, target } = this.props;

    targetOnClick({ event, target });
  }
  handleExit = () => {
    // NOTE: disable FocusLock *before* unmount. animation may end after a new
    // spotlight as gained focus, breaking focus behaviour.
    this.setState({ isExiting: true });
  }

  renderTargetClone() {
    // NOTE: `clone` & `rect` are NOT public API
    const {
      clone, // eslint-disable-line react/prop-types
      pulse,
      rect, // eslint-disable-line react/prop-types
      target,
      targetBgColor,
      targetOnClick,
      targetRadius,
      targetReplacement: Replacement,
    } = this.props;

    if (!target) {
      throw Error(`Spotlight couldn't find a target matching "${target}".`);
    }

    return Replacement ? (
      <Replacement {...rect} />
    ) : (
      <TargetOuter style={rect}>
        <TargetInner pulse={pulse} bgColor={targetBgColor} radius={targetRadius} style={rect}>
          <Clone html={clone} />
          <TargetOverlay onClick={targetOnClick && this.handleTargetClick} />
        </TargetInner>
      </TargetOuter>
    );
  }

  render() {
    // NOTE: `scrollY` & `in` are NOT public API
    const {
      in: transitionIn, scrollY, // eslint-disable-line react/prop-types
      actions, actionsBeforeElement, children, dialogPlacement, dialogWidth,
      footer, header, heading, image,
    } = this.props;

    const { isExiting } = this.state;

    // warn consumers when they provide conflicting props
    if (header && image) {
      console.warn('Please provide "header" OR "image", not both.'); // eslint-disable-line no-console
    }
    if (footer && actions) {
      console.warn('Please provide "footer" OR "actions", not both.'); // eslint-disable-line no-console
    }

    // prepare header/footer elements
    const headerElement = header || (image
      ? <Image alt={heading} src={image} />
      : null);
    const footerElement = footer || (actions
      ? <Actions beforeElement={actionsBeforeElement} items={actions} />
      : null);

    // build the dialog before passing it to Layer
    const dialog = (
      <FocusLock enabled={!isExiting} autoFocus>
        <Dialog width={dialogWidth} tabIndex="-1">
          {headerElement}
          <DialogBody>
            {heading && <Heading>{heading}</Heading>}
            {children}
          </DialogBody>
          {footerElement}
        </Dialog>
      </FocusLock>
    );

    return (
      <Fill in={transitionIn} onExit={this.handleExit} scrollDistance={scrollY}>
        <Layer
          boundariesElement="scrollParent"
          content={dialog}
          offset="0 8"
          position={dialogPlacement}
          zIndex={layers.spotlight(this.props)}
        >
          {this.renderTargetClone()}
        </Layer>
      </Fill>
    );
  }
}

export default withScrollMeasurements(
  withRenderTarget(
    {
      target: 'spotlight',
      withTransitionGroup: false,
    },
    Spotlight
  )
);
