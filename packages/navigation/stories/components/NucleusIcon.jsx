import React from 'react';
import nucleusLogo from '../nucleus.png';

export default () => (
  <img
    src={nucleusLogo}
    alt="Nucleus"
    height="24"
    width="24"
  />
);
