# @atlaskit/field-text-area

## 1.0.3 (2017-11-21)

* bug fix; bumping internal dependencies to latest major versions ([aeebf29](https://bitbucket.org/atlassian/atlaskit/commits/aeebf29))
## 1.0.2 (2017-10-27)

* bug fix; rebuild stories ([7aa7337](https://bitbucket.org/atlassian/atlaskit/commits/7aa7337))


## 1.0.1 (2017-10-22)

* bug fix; update styled component dependency and react peerDep ([39f3286](https://bitbucket.org/atlassian/atlaskit/commits/39f3286))
## 1.0.0 (2017-10-20)

* breaking; Random break to bump to v1 ([d5ebc2e](https://bitbucket.org/atlassian/atlaskit/commits/d5ebc2e))
* breaking; update references to field-text ([d5ebc2e](https://bitbucket.org/atlassian/atlaskit/commits/d5ebc2e))