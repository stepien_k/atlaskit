import React from 'react';
import { StatelessSelect } from '@atlaskit/single-select';

export default (
  <StatelessSelect
    items={[]}
    isOpen
    isLoading
    loadingMessage="Custom loading message"
    shouldFitContainer
  />
);
